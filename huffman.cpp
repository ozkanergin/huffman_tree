#include <fstream>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <cmath>
#include "huffman.h"
#include <algorithm>
#include <string>


using namespace std;

int Huffman::findindex(Token *arr, string elem) {
	// YOU HAVE TO COMPLETE THIS PART
	// THIS FUNCTION RETURNS THE INDEX OF STRING ON TOKEN ARRAY, IF STRING IS NOT IN TOKEN ARRAY, RETURNS -1
	std::string str = elem;
	std::transform(str.begin(), str.end(), str.begin(), ::toupper);

	for (int i = 0; i < token_count; i++)
	{
		if (token_array[i].symbol == str)
		{
			return i;
		}
	}
	return -1;

}

void Huffman::find_frequencies()
{
	//int index = -1;
	//ifstream file_obj; //object to read file
	//file_obj.open("../input.txt", ifstream::in);
	//char symbol = 0;
	//if (!file_obj.is_open()) {
	//	file_obj.open("input.txt", ifstream::in);
	//	if (!file_obj.is_open()) {
	//		cerr << "Could not open the file" << endl;
	//		exit(1);
	//	}
	//}
	//while (file_obj.get(symbol))
	//{
	//	index++;
	//	bool array_include_symbol_b = false;
	//	string a = "";
	//	a.push_back(symbol);

	//	for (int i = 0; i < index; i++)
	//	{
	//		if (token_array[i].symbol == a)
	//		{
	//			array_include_symbol_b = true;
	//			token_array[i].count++;
	//		}
	//	}

	//	if (false == array_include_symbol_b)
	//	{
	//		Token token = Token();
	//		token_array[token_count] = token;
	//		token_array[token_count].symbol = a;
	//		token_array[token_count].count++;
	//		token_count++;
	//	}
	//}

	//for (int i = 0; i < token_count; i++)
	//{
	//	Node* leaf_node = new Node();
	//	leaf_node->token = token_array[i];
	//	priority_queue.push(leaf_node);
	//}

	//file_obj.close();

	string c = "A_DEAD_DAD_CEDED_A_BAD_BABE_A_BEADED_ABACA_BED";
	string s = "";
	for (int i = c.size() - 1;  i >= 0; i--)
	{
		s.push_back(c[i]);
	}
	int index = -1;
	while (false == s.empty())
	{
		s.pop_back();

		index++;
		bool array_include_symbol_b = false;
		string a = "";
		a.push_back(c[index]);

		for (int i = 0; i < index; i++)
		{
			if (token_array[i].symbol == a)
			{
				array_include_symbol_b = true;
				token_array[i].count++;
			}
		}

		if (false == array_include_symbol_b)
		{
			Token token = Token();
			token_array[token_count] = token;
			token_array[token_count].symbol = a;
			token_array[token_count].count++;
			token_count++;
		}
	}

	for (int i = 0; i < token_count; i++)
	{
		Node* leaf_node  = new Node();
		leaf_node->token = token_array[i];
		priority_queue.push(leaf_node);
	}
}

PriorityQueue::PriorityQueue()
{
	head = NULL;
	token_count = 0;
}

void PriorityQueue::push(Node *newnode)
{
	Node* temporary = head;

	if (head == NULL)			// Initialize
	{
		head = newnode;
	}
	else if (newnode->token.count > temporary->token.count)	// Change the head.
	{
		newnode->next = temporary;
		head = newnode;
	}
	else 
	{
		while (temporary != NULL)		// Change the between two nodes or last node
		{
			if (temporary->token.count > newnode->token.count && temporary->next != NULL && temporary->next->token.count < newnode->token.count)
			{
				Node* temp = temporary->next;
				temporary->next = newnode;
				newnode->next = temp;
				break;
			}
			else if (temporary->next == NULL)
			{
				temporary->next = newnode;
				break;
			}
			temporary = temporary->next;
		}
	}
	token_count++;
}

Node* PriorityQueue::pop()
{
	Node* temporary = head;
	Node* pop_node  = head;
	int max = INT_MAX;
	while (temporary != NULL)
	{
		if (temporary->token.count < max)
		{
			pop_node = temporary;
			max = pop_node->token.count;
		}
		temporary = temporary->next;
	}
	Node* former_node = getFormerNode(pop_node);
	Node* next_node   = getNextNode(pop_node);
	former_node->next = next_node;
	token_count--;
	return pop_node;
}

Node* PriorityQueue::getFormerNode(Node* incoming_node) {
	Node* temp = head;
	if (incoming_node == head) return incoming_node;
	while (temp != NULL)
	{
		if (temp->next == incoming_node) {
			return temp;
		}
		temp = temp->next;
	}
	return temp;
}

Node* PriorityQueue::getNextNode(Node* incoming_node) {
	Node* temp = head;
	while (temp != NULL)
	{
		if (temp == incoming_node) {
			return temp->next;
		}
		temp = temp->next;
	}
	return temp->next;
}

void Huffman::get_priority_queue()
{
	Node* temporary = priority_queue.head;
	while (temporary != NULL)
	{
		cout << temporary->token.code.data();
		temporary = temporary->next;
	}
}

HuffmanBinaryTree::HuffmanBinaryTree()
{
	root = NULL;
}

Node * HuffmanBinaryTree::merge(Node *node1, Node *node2)
{
	Node* new_node = new Node();
	new_node->left = node1;
	new_node->right = node2;
	new_node->token.count  = node1->token.count + node2->token.count;
	new_node->token.symbol = node1->token.symbol + node2->token.symbol;
	root = new_node;
	return new_node;
}

void HuffmanBinaryTree::delete_postorder_traversal(Node *traversal)
{
	if (traversal != NULL)
	{
		delete_postorder_traversal(traversal->left);
		delete_postorder_traversal(traversal->right);
		delete traversal;
	}
}

HuffmanBinaryTree::~HuffmanBinaryTree()
{
	delete_postorder_traversal(root);
	//cout << "Code with no memory leak ;)" << endl;
	root = NULL;
}

void Huffman::get_huffman_tree()
{
	// YOU HAVE TO COMPLETE THIS PART 
	while (priority_queue.token_count != 1)
	{
		Node* first_node = priority_queue.pop();
		Node* second_node = priority_queue.pop();

		Node* new_node = huffman_binary_tree.merge(first_node, second_node);
		priority_queue.push(new_node);
	}
}

void Huffman::get_codes(Node *traversal, string codepart)
{
	bool traverse_is_done = true;
	for (int i = 0; i < token_count; i++)
	{
		if (token_array[i].code == "") {
			traverse_is_done = false;
			break;
		}
	}

	if (traverse_is_done == true) 
	{
		return;
	}
	s += codepart;
	stack_nodes.push(traversal);

	if (traversal->left == NULL && traversal->right == NULL && codepart == "0")
	{
		int indeks = findindex(token_array, traversal->token.symbol);
		token_array[indeks].code = s;
		traversal->visited = true;

		s.pop_back();
		stack_nodes.pop();
		last_node = stack_nodes.top();

		if (NULL != last_node->left && NULL != last_node->left && last_node->left->visited == true && last_node->right->visited == true)
		{
			stack_nodes.pop();
			s.pop_back();
			last_node = stack_nodes.top();
			if (last_node->left->visited == true)
			{
				get_codes(last_node->right, "1");
			}
			else
			{
				get_codes(last_node->left, "0");
			}
		}


		get_codes(last_node->right, "1");
	}
	else if (traversal->left == NULL && traversal->right == NULL && codepart == "1")
	{
		int indeks = findindex(token_array, traversal->token.symbol);
		token_array[indeks].code = s;
		traversal->visited = true;
		s.pop_back();
		stack_nodes.pop();
		last_node = stack_nodes.top();

		if (NULL != last_node->left && NULL != last_node->left && last_node->left->visited == true && last_node->right->visited == true)
		{
			stack_nodes.pop();
			s.pop_back();
			last_node = stack_nodes.top();
			if (last_node->left->visited == true)
			{
				get_codes(last_node->right, "1");
			}
			else
			{
				get_codes(last_node->left, "0");
			}
		}
		get_codes(last_node->left, "0");
	}
	traversal->visited = true;

	get_codes(traversal->left , "0");

	// YOU HAVE TO COMPLETE THIS PART
	// A RECURSIVE FUNCTION APPENDS CODEPART STRING 1 OR 0 ACCORDING TO THE HUFFMAN BINARY TREE
}

string Huffman::return_code(string target)
{
	int index = findindex(token_array, target);
	return token_array[index].code;
}

double Huffman::calculate_compression_ratio()
{
	double bit_required_before = 0;
	for (int i = 0; i < token_count; i++)
		bit_required_before += token_array[i].count * 8;
	double bit_required_after = 0;
	for (int i = 0; i < token_count; i++)
		bit_required_after += token_array[i].count * token_array[i].code.length();
	return bit_required_before / bit_required_after;
}
